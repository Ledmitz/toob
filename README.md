# toob


## Name
toob

## Version
1.2.0

## Author and License
Ledmitz, GPL 3.0 (2022)

## Required Apps
bash, zenity, vlc, youtube-dl, GNU coreutils(cat sort tee), awk, grep, sed

## Suggested Apps
wmctrl (required to save window position)

## Description
toob is bash script that should work on most Linux systems, which uses zenity for GUI windows, VLC for playing media and youtube-dl for streams that may require it. The "t"
in toob is intentionally lower case to symbolize humility, since it is only a frontend that makes maintaining a list of streams, AKA channels, and connecting to a video URL
without a browser, very simple. However, there are configurable options that may be desirable to many. Features include: Maintainable Channels selection window with recall
and reload ability, Menu for configuration, size and position of both Menu and Channels windows can be saved, Single URL mode for fast viewing or downloading (youtube-dl
streams only), Alternative youtube site can be saved to convert URLs. e.g. youtu.be/youtube.com --> yewtu.be, Custom user agent strings can be set, editable config with an
undo as well as an option to return to default for some options, channel import (append/overwrite options), channel export (toob or m3u8 format), debug window and ability
to cleanup temporary files as well as automatic backup of Channels before every change (WORK_DIR/channels_bk).

## Tips
toob only provides a few live streams as a demo in channels. I would like to know verified, free to use live streams and provide many more, since toob was made to somewhat
mimic a TV remote. If and until then you will have to find your own. Most have an .m3u8 or .m3u file extension, which may help in your search. A channel can alternatively
be an on demand stream or even a file on your own system, but those would be real rerun channels.

## Installation
Only "toob" is required so I suggest just downloading that file (https://gitlab.com/Ledmitz/toob/-/raw/main/toob). Cloning the git is optional. You may wish to place toob
in a common path. To see paths for the current user `echo $PATH`. A common path for custom scripts on many distros is ~/bin, otherwise you may execute it by its exact
location, wherever you choose. You may need to `sudo chmod +x toob` first. Once toob has permission to execute, I suggest making a panel or desktop launcher.

When toob is first run you'll be asked permission to download the toob icon, channels and any relevant future version's files, which will go directly to the WORK_DIR
(default is ~/toob). This setting will be saved in the configuration file. Any subdirectories or files are generated when needed.

## Usage
Usage: toob / toob [URL/FILE] / toob [OPTION]

  -d, --debug	Show debug log in terminal
  -h, --help	Show this help
  -c, --clear	Delete all files in ~/toob/data, clears debug and youtube-dl cache
  -V, --version	Show toob version
  -w, --window-reset	Delete saved window position entries

toob launches with the Channel window. All settings and channels can be edited from the Menu. All configurations can be undone once using the "Undo Last Change" button
under "Edit Config". If you make an unrecoverable mistake editing a channel, you can import the recent backup from WORK_DIR/channels_bk. Setting the Menu and Channels
windows is probably the first thing you'll want to do, if you have wmctrl installed. If the window does not appear exactly where you set it, you may wish to tinker with the
config as I had to. Both windows were showing as 50 pixels off too low on the Y axis here. The Edit Config option in Menu comes in handy for situations like this. Setting a
custom UA or youtube alternative might be your next step if desired.

toob is designed to be as automatic as possible, detecting if the URL can be played directly or needs youtube-dl. A stream choice window will show if youtube-dl is required,
unless you set a preferred stream when adding a channel (if so, it will play without questions). Often, there are many streams to choose from including audio and video only
streams, depending on the source. toob has filter options to weed out certain streams you may not wish to use and gives you fast access to download your choice. No mp4 to
mp3 conversion required, for instance. Although there is a custom debug window, there is also relevant info in the terminal that could further be used for debugging. toob
also accepts piped input as a URL. Alternatively, a URL can follow the command "toob" and enter Single URL mode. These methods and choosing Single URL from the Menu window
results in toob exiting automatically when the stream is played. If "Single URL" is chosen from the channels window, then it will cycle back as with any other channel.

## Support
https://gitlab.com/Ledmitz/toob/-/issues
I haven't been very prudent in the past on keeping up with these things, but I will try to look more often.
Please keep in mind its just a hobby and that I like to share of course.

## Shadows of What May Be
* A search function would be nice. I have a decent draft already, but not sure if I'll bother adding it.
* Adding ability to download all filetypes would also be nice and easy. In the meantime wget is a great alternative on its own. If you need the URL of the current video,
just check debug, but don't be suprised if its just a playlist file (m3u8). VLC has a record feature you may wish to make use of for these cases.
* Create a channels file with many known to be free to use streams

## Contributing
Here's where I look really green. I'm not even sure how to add a merge request or fix a conflict at this point, but if I get any MRs, I'll have to look into it if they
seem to be improvements I can understand. With that in mind, I'd like to keep toob very simple. I feel flashy stuff causes useless overhead so I'm a bit of a minimalist
and there's only so much configuration one can do with zenity anyway, but there are other more customizable applications that would be fun to try. They are only useful
if fairly, universally available in most distros though. If anyone out there feels like packaging toob for specific distros, that would be a great and welcome contribution.

## Changes
* 2023-01-07 - Added Reload function for streams that freeze, Made Recall and Reload both buttons instead of channels to select, Added missing changed variables to Edit Config function (Changes did not occur without an app restart before)
* 2022-12-28(Recall_Fix) - When recalling back and forth between channels a few times, the recall value would appear as the current playing channel. This has been fixed.
* 2022-12-28 - Added ability to recall the last channel and fixed issue of not being prompted for download of youtube-dl streams while in Single URL mode (Downloads
are/will not be available to Single URL in channels)


